# Diversity Web template
[Diversify](http://webdesign-finder.com/html/diversify/) community web template.

## Screen shots
![](screen_shots/home-1.png)

![](screen_shots/home-2.png)

![](screen_shots/home-3.png)

![](screen_shots/home-4.png)

![](screen_shots/home-5.png)

![](screen_shots/home-6.png)

![](screen_shots/home-7.png)

![](screen_shots/blog-1.png)

![](screen_shots/blog-2.png)

![](screen_shots/shop-1.png)

![](screen_shots/shop-2.png)